﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Proektirovanie
{
    public partial class Find_prod : Form
    {
        public string ID_text
        {
            get { return textBox1.Text; }
            set { textBox1.Text = value; }
        }
        public string Type_text
        {
            get { return textBox2.Text; }
            set { textBox2.Text = value; }
        }
        public string Name_Firm_text
        {
            get { return textBox3.Text; }
            set { textBox3.Text = value; }
        }
        public string Novinka
        {
            get { return textBox4.Text; }
            set { textBox4.Text = value; }
        }
        public Find_prod()
        {
            InitializeComponent();
        }
        private void Find_prod_Load(object sender, EventArgs e)
        {
            textBox1.Enabled = false;
            textBox2.Enabled = false;
            textBox3.Enabled = false;
            textBox4.Enabled = false;
            radioButton1.Checked = false;
        }
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
                textBox1.Enabled = true;               
        }
        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked)
                textBox2.Enabled = true;
        }
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked)
                textBox3.Enabled = true;
        }
        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked)
                textBox4.Enabled = true;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
