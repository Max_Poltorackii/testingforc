﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Proektirovanie
{
    public partial class Form1 : Form
    {
        Sklad skl1;
        private Find_prod F1 = new Find_prod();
        private AddProducts F2 = new AddProducts();
        public Form1()
        {
            InitializeComponent();
        }
        private void Sklad()
        {
            skl1 = new Sklad("Склад №1", 1000, 700);

            ProdyktTypes prod1 = new ProdyktTypes(1,"Молоко", 50,true,"Колхоз №5");
            ProdyktTypes prod2 = new ProdyktTypes(3,"Хлеб", 50,false,"Херсонский Хлеб завод");
            ProdyktTypes prod3 = new ProdyktTypes(4,"Табак", 25,false,"ЧП Паша Срибный");
            ProdyktTypes prod4 = new ProdyktTypes(2,"Алкоголь", 25,true,"ЧП Алко-мир");
            ProdyktTypes prod5 = new ProdyktTypes(5,"Сок", 100, false, "Сандора");
            ProdyktTypes prod6 = new ProdyktTypes(9,"Конфеты", 25, true, "Рошен");
            ProdyktTypes prod7 = new ProdyktTypes(6,"Помидоры",100, false,"Колхоз №5");
            ProdyktTypes prod8 = new ProdyktTypes(10,"Колбаса",100, true, "Херсонский мясо комбинат");
            ProdyktTypes prod9 = new ProdyktTypes(7,"Яйца",50, false, "Птице-фаюрека");
            ProdyktTypes prod10 = new ProdyktTypes(8,"Сахар",50, true, "ЧП Сладклежка");

            skl1.arTheSklad.Add(prod1);
            skl1.arTheSklad.Add(prod2);
            skl1.arTheSklad.Add(prod3);
            skl1.arTheSklad.Add(prod4);
            skl1.arTheSklad.Add(prod5);
            skl1.arTheSklad.Add(prod6);
            skl1.arTheSklad.Add(prod7);
            skl1.arTheSklad.Add(prod8);
            skl1.arTheSklad.Add(prod9);
            skl1.arTheSklad.Add(prod10);
        }
        private void Find_Products_ID()
        {   
            foreach (ProdyktTypes res in skl1.arTheSklad)
                if (res.ID == int.Parse(F1.ID_text))
                {
                    richTextBox1.Text += "ID продукта" + res.ID + "\n" +
                    "Тип продукта:" + res.Type + "\n" +"Изготовитель:" + res.Frim + "\n" +
                   "Дата" + res.Novinka.ToString() + "\n" +"Количество продукта на скаде" + 
                   res.Kolvo.ToString();
                }
        }
        private void Find__Type() 
        {
            string type_produkts;
            type_produkts = F1.Type_text;
            var Query1 = from c in skl1.arTheSklad
                         where c.Type == type_produkts
                         select c;
            foreach (var res in Query1)
            {
                richTextBox1.Text += "ID продукта:" + res.ID.ToString() + "\n" +
                "Тип продукта:" + res.Type + "\n" +"Изготовитель:" + res.Frim + "\n" +
                "Дата" + res.Novinka.ToString() + "\n" +"Количество продукта на скаде" + 
                res.Kolvo.ToString();
            }     
        }
        private void UpdateGrid()
        {       
                if (skl1.arTheSklad != null)
                {
                    DataTable inventory = new DataTable("Produkts");
                    DataColumn Name_ = new DataColumn("ID");
                    DataColumn Nov = new DataColumn("Новинка");
                    DataColumn name_frim = new DataColumn("Изготовитель");
                    DataColumn Produkts_now_ = new DataColumn("Тип");
                    DataColumn Count_ = new DataColumn("Количество");
                    DataColumn data_ = new DataColumn("Дата принятия товара");

                    inventory.Columns.Add(Name_);
                    inventory.Columns.Add(Produkts_now_);
                    inventory.Columns.Add(name_frim);
                    inventory.Columns.Add(data_);
                    inventory.Columns.Add(Count_);
                    inventory.Columns.Add(Nov);

                    foreach (ProdyktTypes produkt in skl1.arTheSklad)
                    {
                        DataRow _newRowForSklad;
                        _newRowForSklad = inventory.NewRow();
                        _newRowForSklad["ID"] = produkt.ID;
                        _newRowForSklad["Дата принятия товара"] = "15.16.01";
                        _newRowForSklad["Изготовитель"] = produkt.Frim;
                        _newRowForSklad["Новинка"] = produkt.Novinka;
                        _newRowForSklad["Тип"] = produkt.Type;
                        _newRowForSklad["Количество"] = produkt.Kolvo;
                        inventory.Rows.Add(_newRowForSklad);
                    }
                    dataGridView1.DataSource = inventory;
                }
            }
        public void Save_XML()
        {
            DataTable table = new DataTable();
            DataColumn table_ID = new DataColumn("ID ");
            DataColumn table_Now = new DataColumn("Новинка");
            DataColumn table_Firm = new DataColumn("Изготовитель");
            DataColumn table_Tap = new DataColumn("Тип продукта");
            DataColumn table_Kol = new DataColumn("Количество");
                  
            table.Columns.Add(table_ID);
            table.Columns.Add(table_Now);
            table.Columns.Add(table_Firm);
            table.Columns.Add(table_Tap);
            table.Columns.Add(table_Kol);

            table.Rows.Add(skl1.IDSklada, skl1.ProdNowOnSkald, skl1.ProdOnSkald);
            foreach (ProdyktTypes res in skl1.arTheSklad)
                table.Rows.Add(res.ID, res.Novinka, res.Frim, res.Type, res.Kolvo);
           
            DataSet set = new DataSet();
            set.Tables.Add(table);
            set.WriteXml("CreatedXML.xml");
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Sklad();
            UpdateGrid();   
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            F1.ShowDialog();
            if (F1.ID_text.Length == 0)
                Find__Type();
            else if (F1.Type_text.Length == 0)
                Find_Products_ID();
        }
        private void button3_Click_1(object sender, EventArgs e)
        {
            try
            {
                F2.ShowDialog();
                ProdyktTypes new_prod = new ProdyktTypes(int.Parse(F2.Tex1), F2.Tex2, int.Parse(F2.Tex3), bool.Parse(F2.Tex4),
                    F2.Tex5);
                int count = 0;
                foreach (ProdyktTypes res in skl1.arTheSklad)
                    count += res.Kolvo;
                if (count >= skl1.ProdOnSkald)
                    richTextBox1.Text += "Извините склад переполнен!";
                else
                {
                    richTextBox1.Text += count.ToString();
                    skl1.arTheSklad.Add(new_prod);
                }
                UpdateGrid();
            }
            catch
            {
                richTextBox1.Text += "Добавти продукты";
            }   
        }
        private void button2_Click_1(object sender, EventArgs e)
        {
            timer1.Start();
        }
        private void button5_Click(object sender, EventArgs e)
        {
            richTextBox1.Text= "  ";
        }
        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            progressBar1.Increment(+1);
            if (progressBar1.Value == 99)
                Save_XML();
        }
    }
}
