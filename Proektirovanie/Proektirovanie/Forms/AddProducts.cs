﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Proektirovanie
{
    public partial class AddProducts : Form
    {
        public string Tex1
        {
            get { return textBox1.Text; }
            set { textBox1.Text = value; }
        }
        public string Tex2
        {
            get { return textBox2.Text; }
            set { textBox2.Text = value; }
        }
        public string Tex3
        {
            get { return textBox3.Text; }
            set { textBox3.Text = value; }
        }
        public string Tex4
        {
            get { return textBox4.Text; }
            set { textBox4.Text = value; }
        }
        public string Tex5
        {
            get { return textBox5.Text; }
            set { textBox5.Text = value; }
        }
        public AddProducts()
        {
            InitializeComponent();
        }

        private void AddProducts_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
