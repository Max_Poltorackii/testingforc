﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proektirovanie
{
   public class ProdyktTypes
    {
        private int _id;
        private string _type;
        private int _kolvo;
        private bool _novinka;
        private string _name_frim;

        public int ID
        {
            get { return _id;}
            set { _id = value; }
        }
        public int Kolvo
        {
            get { return _kolvo; }
            set { _kolvo = value; }
        }
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public bool Novinka
        {
            get { return _novinka; }
            set { _novinka = value; }
        }
        public string Frim
        {
            get { return _name_frim; }
            set { _name_frim = value; }
        }
        public ProdyktTypes(int _id, string _type, int _kolvo, bool _novinka, string _name_frim)
        {
            this._id = _id;
            this._type = _type;
            this._kolvo = _kolvo;
            this._novinka = _novinka;
            this._name_frim = _name_frim;
        }
    }
}
