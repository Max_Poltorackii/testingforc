﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Proektirovanie
{
    public class Sklad
    {
        private string _idSklad;
        private int _prodOnSkald;
        private int _countnowonsklad;
        public List<ProdyktTypes> arTheSklad;

        public string IDSklada
        {
            get { return _idSklad; }
            set { _idSklad = value; }
        }
        public int ProdOnSkald
        {
            get { return _prodOnSkald; }
            set { _prodOnSkald = value; }
        }
        public int ProdNowOnSkald
        {
            get { return _countnowonsklad; }
            set { _countnowonsklad = value; }
        }
        public Sklad(string _idSklad, int _prodOnSkald, int _countnowonsklad)
        {
            this._idSklad = _idSklad;
            this._prodOnSkald = _prodOnSkald;
            this._countnowonsklad =_countnowonsklad;
            arTheSklad = new List<ProdyktTypes>();    
        }
    }
}
