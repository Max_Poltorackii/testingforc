﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Xml.Linq;
using System.Xml;
using System.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpf_linq
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
             

        }

        private void button1_Click_1(object sender, RoutedEventArgs e)
        {   
            
            DataTable table = new DataTable();
            DataColumn table_Colum_Name = new DataColumn("Name");
            DataColumn table_Colum_Surname = new DataColumn("Surname");
            DataColumn table_Colum_Age = new DataColumn("Age");
            DataColumn table_Colum_Adress = new DataColumn("Addres");

            table.Columns.Add(table_Colum_Name);
            table.Columns.Add(table_Colum_Surname);
            table.Columns.Add(table_Colum_Age);
            table.Columns.Add(table_Colum_Adress);
            string t = "Thi is C#";
            table.Rows.Add("Максим", "Полторацкий", "20", "г.Херсон ул. Нефтяников 61 кв.9");
            table.Rows.Add("Денис", "Полторацкий", "19", "г.Херсон ул. Гмырева 8 кв.19");
            table.Rows.Add("Саня", "Пaнасенко", "20", "г.Херсон ул. Краснофлотская 18");
            table.Rows.Add("Саня", "Сикоза", "19", "г. Херсон, ул. Краснофлотская 8 ком. 15");
            table.Rows.Add("Вика", "Рогач", "19", "г.Цурюпинск ул. карла Маркса 29 кв.5");
            table.Rows.Add("Валера", "Ефименко", "21", "г.Херсон ул. Покрешева 15 кв.91");

            DataSet set = new DataSet();

            set.Tables.Add(table);
            set.WriteXml("CreatedXML.xml");
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            string Template = " ";
        }

        private void listView1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {   
         
            var doc = XDocument.Load("CreatedXML.xml");
            var num1 = from c in doc.Descendants("Table1")
                       group c by c.Element("Name").Value == "Саня" into g
                       select new
                       {
                           Name = g.Key,
                           Count_person = g.Count(c => c.Element("Name").Value == "Саня"),
                           Max_Age = g.Max(c => c.Element("Age").Value)
                       };
            var num2 = from p in num1
                       where p.Count_person == 2
                       select new
                       {

                           p.Count_person,
                           p.Max_Age
                       };
            foreach (var res in num2)
                textBox1.Text += ("Количество елементво в групе" + res.Count_person +"\n"+
                                 "Макс возраст в группе" + res.Max_Age +"\n"+
                                 "Имя");
           
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

      /*  private void button6_Click(object sender, RoutedEventArgs e)
        {
            var doc = XDocument.Load("CreatedXML.xml");
            var num1 = from c in doc.Descendants("Table1")
                       group c by c.Element("Name").Value == "Саня" into g
                       select new
                       {
                           Name = g.Key,
                           Count_person = g.Count(c => c.Element("Name").Value == "Саня"),
                           Max_Age = g.Max(c => c.Element("Age").Value)
                       };
            var num2 = from p in num1
                       where p.Count_person == 2
                       select new
                       {

                           p.Count_person,
                           p.Max_Age
                       };
            foreach (var res in num2)
                textBox1.Text += ("Количество елементво в групе" + res.Count_person + "\n" +
                                 "Макс возраст в группе" + res.Max_Age + "\n" +
                                 "Имя");
        }*/
    }
}
