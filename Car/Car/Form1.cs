﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
namespace Car
{
//test
    public partial class Form1 : Form
    {
        public List<Sklad> arTheSklad=null ;
        public Form1()
            
        {
            InitializeComponent();
            CenterToScreen();
            arTheSklad = new List<Sklad>();    
        }
        private void Update_Grid()
        {
            if (arTheSklad != null)
            {
                DataTable inventory = new DataTable("Sklad");
                DataColumn Name_ = new DataColumn("Name");
                DataColumn Type_ = new DataColumn("Type");
                DataColumn ID_ = new DataColumn("ID");
                DataColumn Ton_ = new DataColumn("Tonajnos");
                DataColumn Flag_ = new DataColumn("Flag");
                DataColumn Kolvo_now_ = new DataColumn("Now Element");

                inventory.Columns.Add(Name_);
                inventory.Columns.Add(Type_);
                inventory.Columns.Add(ID_);
                inventory.Columns.Add(Ton_);
                inventory.Columns.Add(Flag_);
                inventory.Columns.Add(Kolvo_now_);


                foreach (Sklad res_sklad in arTheSklad)
                {
                    DataRow _newRowForSklad;
                    _newRowForSklad = inventory.NewRow();
                    _newRowForSklad["Name"] = res_sklad.Name;
                    _newRowForSklad["Type"] = res_sklad.Type;
                    _newRowForSklad["ID"] = res_sklad.ID;
                    _newRowForSklad["Tonajnos"] = res_sklad.Kol_element;
                    _newRowForSklad["Flag"] = res_sklad.Flag;
                    _newRowForSklad["Now Element"] = res_sklad.Now_Kol;
                    inventory.Rows.Add(_newRowForSklad);
                }

                CarsGrid.DataSource = inventory;
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Update_Grid();
        }
        private void Perevozki()
        {  
            int id1 = int.Parse(textBox1.Text);
            int id2 = int.Parse(textBox2.Text);
            int kolvo = int.Parse(textBox5.Text);

            var Num1 = from c in arTheSklad
                      where c.ID == id1
                      select new
                      {
                       kolvo=c.Kol_element-kolvo
                      };
            foreach (var res in Num1) 
                richTextBox1.Text += res.kolvo + "\n";

            var Num2 = from c in arTheSklad
                       where c.ID == id2
                       select new
                       {
                           kolvo1 = c.Kol_element+kolvo
                       };
            foreach (var res in Num2)
                richTextBox1.Text += res.kolvo1 + "\n";

            Update_Grid();
        }
        private void button2_Click(object sender, EventArgs e)
        {
            int id1 = int.Parse(textBox1.Text);
            int id2 = int.Parse(textBox2.Text);
            int kolvo = int.Parse(textBox5.Text);
            string type_ssear = textBox3.Text;

            int temp=0;
            foreach (Sklad res in arTheSklad)
            {
                if (((type_ssear == res.Type) && (res.ID == id1)) || ((type_ssear == res.Type) && (res.ID == id2)))
                {
                    temp += 1;
                    if (temp % 2 == 0)
                    {
                        richTextBox1.Enabled = true;

                        richTextBox1.Text+= "Перевозка возможна" + "\n";
                        richTextBox1.Text+= "*************************************" + "\n";
                        richTextBox1.Text+= "Отправленно " + kolvo +""+"тон" + "\n";
                        richTextBox1.Text += "Отпрoвитель ID=" + " " + id1 + "\n";
                        richTextBox1.Text += "Получатель ID=" + " " + id2 + "\n";
                        richTextBox1.Text += "Тип товара:" + type_ssear + "\n";
                        Perevozki();
                        break;      
                    }
                }
 
            }
        }
        private void Sklad()
        {
            richTextBox1.Enabled = false;
  
            Sklad Sklad_1 = new Sklad("Cклад номер 1", "Хлеб", 1,150, false,10);
            Sklad Sklad_2 = new Sklad("Cклад номер 2", "Холодильники", 2, 100, false, 50);
            Sklad Sklad_3 = new Sklad("Cклад номер 3", "Компьютер", 3, 200, false, 0);
            Sklad Sklad_4 = new Sklad("Cклад номер 4", "Алкоголь", 7, 140, false, 90);
            Sklad Sklad_5 = new Sklad("Cклад номер 5", "Молоко", 5,150, false, 0);
            Sklad Sklad_6 = new Sklad("Cклад номер 6", "Алкоголь", 6,200, false, 00);
            Sklad Sklad_7 = new Sklad("Cклад номер 7", "Хлеб", 4,350, false, 0);
            Sklad Sklad_8 = new Sklad("Cклад номер 8","Молоко", 10,550,false, 150);
            Sklad Sklad_9 = new Sklad("Cклад номер 9", "Компьютер", 8, 100,false, 0);
            Sklad Sklad_10 =new Sklad("Cклад номер 10", "Холодильники", 9, 350, false, 0);
            Sklad Sklad_11 =new Sklad("Cклад номер 11", "Дрова", 10, 150, false, 10);
            Sklad Sklad_12 =new Sklad("Cклад номер 12", "Дрова", 11, 100, false, 50);

            arTheSklad.Add(Sklad_1);
            arTheSklad.Add(Sklad_3);
            arTheSklad.Add(Sklad_4);
            arTheSklad.Add(Sklad_5);
            arTheSklad.Add(Sklad_6);
            arTheSklad.Add(Sklad_7);
            arTheSklad.Add(Sklad_8);
            arTheSklad.Add(Sklad_9);
            arTheSklad.Add(Sklad_10);
            arTheSklad.Add(Sklad_11);
            arTheSklad.Add(Sklad_12);
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Sklad();
        }
        public void XML()
        {
            DataTable table = new DataTable();

            DataColumn table_Colum_Name = new DataColumn("Name");
            DataColumn table_Colum_Surname = new DataColumn("ID");
            DataColumn table_Colum_Age = new DataColumn("Kol");
            DataColumn table_Colum_Adress = new DataColumn("Type");
            DataColumn tableFlag = new DataColumn("Flag");
            DataColumn tableKol = new DataColumn("Kol now");

            table.Columns.Add(table_Colum_Name);
            table.Columns.Add(table_Colum_Surname);
            table.Columns.Add(table_Colum_Age);
            table.Columns.Add(table_Colum_Adress);
            table.Columns.Add(tableKol);
            table.Columns.Add(tableFlag);

            foreach (Sklad res in arTheSklad)
                table.Rows.Add(res.Name, res.ID, res.Kol_element, res.Type, res.Now_Kol, res.Flag);
        
            DataSet set = new DataSet();
            set.Tables.Add(table);
            set.WriteXml("CreatedXML.xml");
        }
        private void button4_Click(object sender, EventArgs e)
        {
            XML();
        }
        private void button3_Click(object sender, EventArgs e)
        {
            Perevozki();
        }
    /*    private void button2_Click(object sender, EventArgs e)
        {
            AddCars d = new AddCars();
            if (d.ShowDialog()==DialogResult.OK)
            {
                arTheSklad.Add(d.The_Cars);
                Update_Grid();
            }
        }*/

        
    }
}
