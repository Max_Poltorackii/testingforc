﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Car
{
      public  class Sklad
    {
       private string _name;
       private string _type;
       private int _id;
       private int _kolvo_element;
       private bool _flag;
       private  int _kolovo_now;

       public Sklad(string _name, string _type, int _id, int _kolvo_element, bool _flag, int _kolvo_now)
       {
           this._name = _name;
           this._type = _type;
           this._id = _id;
           this._kolvo_element = _kolvo_element;
           this._flag = _flag;
           this._kolovo_now = _kolovo_now;
       }

       public string Name
       {
           get
           {
               return _name;
           }
           set
           {
               value = _name;
           }
       }
       public string Type
       {
           get
           {
               return _type;
           }
           set
           {
               value = _type;
           }
       }
       public int ID
       {
           get
           {
               return _id;
           }
           set
           {
               value = _id;
           }
       }
       public int Kol_element
       {
           get
           {
               return _kolvo_element;
           }
       }
       public bool Flag
       {
           get
           {
               return _flag;
           }
           set
           {
               value = _flag;
           }
       }
       public int Now_Kol
       {
           get
           {
               return _kolovo_now;
           }
           set
           {
               value = _kolovo_now;
           }
       }

    }
}
